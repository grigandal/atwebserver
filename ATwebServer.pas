unit ATwebServer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdTCPServer,
  IdCustomHTTPServer, IdHTTPServer, TlHelp32, cUnicodeCodecs, StrUtils,
  ExtCtrls;

const
  LogName = '\server.log';
  EsName = 'es.exe';
type
  RPIPEMessage = record
    Size: DWORD;
    Kind: Byte;
    Count: DWORD;
    Data: array[0..318095] of Char;
end;



type
  ConnectionInfo = record
    IP: string;
    sessionID: string;
    PID: Cardinal;
end;

type
  TAThttpServer = class(TForm)
    WebServer: TIdHTTPServer;
    Button: TButton;
    Label1: TLabel;
    docroot_edit: TEdit;
    webes_edit: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    waittime_edit: TEdit;
    Label4: TLabel;
    procedure ButtonClick(Sender: TObject);
    procedure WebServerCommandGet(AThread: TIdPeerThread;
      ARequestInfo: TIdHTTPRequestInfo;
      AResponseInfo: TIdHTTPResponseInfo);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    ConnectionList: TList;
    CreatingProcess: boolean;
    DocRoot, EsPath : string;
    FPipeName: string;
    SPipeName:integer;
    WaitTime: integer;
    procedure Connect(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure Disconnect(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure GetForm(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure Log(s:string);
    function KillTask(ExeFileName: string):boolean;
    procedure CalcMsgSize(var Msg: RPIPEMessage);
    function ProcessPipeMsg (aMsg: RPIPEMessage; PipeName:String): RPIPEMessage;
    function SendPipeString(aStr: String; PipeName:String): String;
    function ProcessTerminate(dwPID:Cardinal):Boolean;
    function ConnectionCheck(ARequestInfo: TIdHTTPRequestInfo):Boolean;
    { Public declarations }
  end;

var
  AThttpServer: TAThttpServer;

implementation

{$R *.dfm}

procedure TAThttpServer.CalcMsgSize(var Msg: RPIPEMessage);
begin
  Msg.Size :=
    SizeOf(Msg.Size) +
    SizeOf(Msg.Kind) +
    SizeOf(Msg.Count) +
    Msg.Count +
    3;
end;

function TAThttpServer.ProcessPipeMsg(aMsg: RPIPEMessage; PipeName:string): RPIPEMessage;
begin
  CalcMsgSize(aMsg);

  Result.Size := SizeOf(Result);
  if WaitNamedPipe(PChar(PipeName), 10000) then
    if not CallNamedPipe( PChar(PipeName), @aMsg, aMsg.Size, @Result,
    											Result.Size, Result.Size, 10000 ) then
      raise Exception.Create('PIPE did not respond.')
    else
  else
    raise Exception.Create('PIPE does not exist.');
end;

function TAThttpServer.SendPipeString(aStr: String; PipeName:String): String;
var
  Msg: RPIPEMessage;
begin
  // prepare outgoing message
  Msg.Kind := 1;
  Msg.Count := Length(aStr);
  StrPCopy(Msg.Data, aStr);
  // send message
  Msg := ProcessPipeMsg(Msg,PipeName);
  // return data send from server
  Result := Copy(Msg.Data, 1, Msg.Count);
end;



procedure TAThttpServer.ButtonClick(Sender: TObject);
var Log:TextFile;
begin
  if (Button.Caption = '��������� ���-������') then
  begin
    CreatingProcess:=false;
    ConnectionList:=Tlist.Create;
    SPipeName:=0;
    FPipeName:= '\\.\pipe\'+intToStr(SPipeName);
    KillTask(EsName);
    DocRoot:= docroot_edit.Text;
    EsPath:= webes_edit.Text;
    try
    WaitTime:=strToInt(waittime_edit.Text)
    except
    on E: EConvertError do
     begin
     ShowMessage('������� ��������� ����� �������� ���������(� ��)');
     exit;
     end;
    end;
    if not FileExists(DocRoot+LogName) then
    begin
      AssignFile( Log, DocRoot+LogName );
      Rewrite(Log);
      CloseFile(Log);
    end;

    WebServer.Active:= True;
    Button.Caption:= '���������� ������ ���-�������';
  end
  else
  begin
    KillTask(EsName);
    ConnectionList.Destroy;
    WebServer.Active:= False;
    Button.Caption:= '��������� ���-������';
  end;

end;

function TAThttpServer.KillTask(ExeFileName: string): boolean;
const
PROCESS_TERMINATE=$0001;
var
ContinueLoop: BOOL;
FSnapshotHandle: THandle;
FProcessEntry32: TProcessEntry32;
begin
result := false;

FSnapshotHandle := CreateToolhelp32Snapshot
(TH32CS_SNAPPROCESS, 0);
FProcessEntry32.dwSize := Sizeof(FProcessEntry32);
ContinueLoop := Process32First(FSnapshotHandle,
FProcessEntry32);

while integer(ContinueLoop) <> 0 do
begin
if ((UpperCase(ExtractFileName(FProcessEntry32.szExeFile)) =
UpperCase(ExeFileName)) or (UpperCase(FProcessEntry32.szExeFile) =
UpperCase(ExeFileName))) then
Result := (TerminateProcess(OpenProcess(
PROCESS_TERMINATE, BOOL(0), FProcessEntry32.th32ProcessID), 0));
ContinueLoop := Process32Next(FSnapshotHandle, FProcessEntry32);
end;
CloseHandle(FSnapshotHandle);
end;


procedure TAThttpServer.Connect;
var
		SI: TStartupInfo;
		PI: TProcessInformation;
    CI: ^ConnectionInfo;
    i:integer;
begin
  for i := 0 to ConnectionList.Count-1 do
  begin
    CI:=ConnectionList[i];
    if CI^.IP = ARequestInfo.RemoteIP then
    begin
      ProcessTerminate(CI^.PID);
      ConnectionList.delete(i);
      log(CI^.IP+' is reconnected. Session #'+CI^.sessionID+' is terminated');
    end;
  end;

    while  CreatingProcess do
     begin
     sleep(500);
     log('sleep');
     end;
    CreatingProcess:=true;
    SPipeName:=SPipeName+1;
    Log('connect N'+inttostr(SPipeName));

    FillChar(SI,SizeOf(TStartupInfo),#0);
    FillChar(PI,SizeOf(TProcessInformation),#0);
    SI.cb := SizeOf(TStartupInfo);

    if not CreateProcess(nil, PChar(EsPath+'\'+EsName+' '+intToStr(SPipeName)), nil, nil, false, CREATE_NEW_PROCESS_GROUP+NORMAL_PRIORITY_CLASS, nil, PChar(EsPath+'\'), SI, PI) then
    begin
      Log(EsPath+'\'+EsName + ' running error ' + IntToStr(GetLastError()));
    end
    else
    begin
	    Log(EsPath+'\'+EsName + ' with session #'+inttostr(SPipeName)+ ' is started' );
      sleep(WaitTime);
      CreatingProcess:=false;
    end;
    new(CI);
    CI^.IP:= ARequestInfo.RemoteIP;
    CI^.PID:= PI.dwProcessId;
    CI^.sessionID:= intToStr(SPipeName);
    ConnectionList.Add(CI);
    CloseHandle(PI.hProcess);
    CloseHandle(PI.hThread);

    AResponseInfo.ContentType:= 'text/html';
    AResponseInfo.ContentEncoding:= 'UTF-8';
    AResponseInfo.CacheControl:= 'no-cache, must-revalidate';
    AResponseInfo.Pragma:= 'no-cache';
    AResponseInfo.ContentText := WideStringToUTF8String('<webform><accept session_id="'+
    inttostr(SPipeName)+'"></accept></webform>');
    AResponseInfo.WriteHeader;
    AResponseInfo.WriteContent;
end;

procedure TAThttpServer.Disconnect;
var sessionid,PipeName : string;
i:integer;
CI:^connectionInfo;
begin
    sessionid:=ARequestInfo.Params.values['session_id'];
    PipeName:=  '\\.\pipe\'+sessionid;
    if not ConnectionCheck(ARequestInfo) then
    begin
    log('������ '+sessionid+'  IP '+ARequestInfo.RemoteIP+'  ��� �������');
    exit;//������ ��� �������.
    end;
{    index:=ARequestInfo.Params.IndexOfName('session_id');
    ARequestInfo.Params.delete(index);
    ARequestInfo.Params.Delimiter:= '&';
    Request:= ARequestInfo.Params.DelimitedText;
}
   // Request:='disconnect';
   // Response:= SendPipeString(Request+#0, PipeName);
    for i := 0 to ConnectionList.Count-1 do
    begin
    CI:=ConnectionList[i];
      if (CI^.sessionID = sessionid) then
      begin
        log('terminate on disconnect. IP: '+CI^.IP+' ID: '+CI^.sessionID+'  PID: '+IntToStr(CI^.PID));
        ProcessTerminate(CI^.PID);
      end;
    end;
end;

procedure TAThttpServer.Getform;
var Request, Response,sessionid,PipeName : string;
index: integer;
begin
    if not ConnectionCheck(ARequestInfo) then
    begin

    log('������ ���������� �� ��������������');
    AResponseInfo.ContentType:= 'text/html';
    AResponseInfo.ContentEncoding:= 'UTF-8';
    AResponseInfo.CacheControl:= 'no-cache, must-revalidate';
    AResponseInfo.Pragma:= 'no-cache';
    AResponseInfo.ContentText := (WideStringToUTF8String('<WebForm><Panel title="������ ������ �������. �������� �������� ��� ������ ������."></Panel></WebForm>'));
    AResponseInfo.WriteHeader;
    AResponseInfo.WriteContent;

    exit;
    end;
    sessionid:=ARequestInfo.Params.values['session_id'];
    PipeName:=  '\\.\pipe\'+sessionid;
    index:=ARequestInfo.Params.IndexOfName('session_id');
    ARequestInfo.Params.delete(index);
    ARequestInfo.Params.Delimiter:= '&';
    Request:= ARequestInfo.Params.DelimitedText;
    Response:= SendPipeString(Request+#0, PipeName);
    AResponseInfo.ContentType:= 'text/html';
    AResponseInfo.ContentEncoding:= 'UTF-8';
    AResponseInfo.CacheControl:= 'no-cache, must-revalidate';
    AResponseInfo.Pragma:= 'no-cache';
    AResponseInfo.ContentText := (WideStringToUTF8String(Response));
    AResponseInfo.WriteHeader;
    AResponseInfo.WriteContent;
 //   Log('response '+Response);
 { for i := 0 to ConnectionList.Count-1 do
  begin
   CI:=ConnectionList[i];

    log(CI^.IP+' is ID '+CI^.sessionID+'   '+IntToStr(CI^.PID));
  end;

  }

end;

procedure TAThttpServer.Log(s:string);
var
  Log: TextFile;
begin
  AssignFile( Log, DocRoot+LogName );
  Append( Log );
  Write( Log, DateTimeToStr(Now));
  WriteLn( Log, ' : ' + s);
  CloseFile( Log );
end;


procedure TAThttpServer.WebServerCommandGet(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
begin
   log('request from IP:'+ARequestInfo.RemoteIP +'   '+ARequestInfo.Document);
   if pos('/webies/connect', ARequestInfo.Document) = 1 then
   begin
      Connect(AThread, ARequestInfo, AResponseInfo);
   end
   else if pos('/webies/disconnect', ARequestInfo.Document) = 1 then
   begin
      Disconnect(AThread, ARequestInfo, AResponseInfo);
   end
   else if pos('/webies/getform', ARequestInfo.Document) = 1 then
   begin
      GetForm(AThread, ARequestInfo, AResponseInfo);
   end else if FileExists(DocRoot+ARequestInfo.Document) then
   begin

      if RightStr(ARequestInfo.Document,4) = '.swf' then
        AResponseInfo.ContentType:= 'application/x-shockwave-flash'
      else if RightStr(ARequestInfo.Document,4) = '.jpg' then
        AResponseInfo.ContentType:= 'image/jpeg'
      else if RightStr(ARequestInfo.Document,4) = '.png' then
        AResponseInfo.ContentType:= 'image/png'
     else if RightStr(ARequestInfo.Document,4) = '.gif' then
        AResponseInfo.ContentType:= 'image/gif';

     AResponseInfo.CacheControl:= 'no-cache, must-revalidate';
     AResponseInfo.Pragma:= 'no-cache';
     WebServer.ServeFile(AThread, AResponseInfo, DocRoot+ARequestInfo.Document);
   end;

end;


function TAThttpServer.ProcessTerminate(dwPID:Cardinal):Boolean;
 var
// hToken:THandle;
// SeDebugNameValue:Int64;
// tkp:TOKEN_PRIVILEGES;
// ReturnLength:Cardinal;
 hProcess:THandle;
 begin
 Result:=false;
 // ��������� ���������� SeDebugPrivilege
 // ��� ������ �������� ����� ������ ��������
 //if not OpenProcessToken( GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES
 //or TOKEN_QUERY, hToken ) then
//  exit;

 // �������� LUID ����������
// if not LookupPrivilegeValue( nil, 'SeDebugPrivilege', SeDebugNameValue )
// then begin
// CloseHandle(hToken);
// exit;
// end;

// tkp.PrivilegeCount:= 1;
// tkp.Privileges[0].Luid := SeDebugNameValue;
 //tkp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;

 // ��������� ���������� � ������ ��������
// AdjustTokenPrivileges(hToken,false,tkp,SizeOf(tkp),tkp,ReturnLength);
 //if GetLastError()< > ERROR_SUCCESS then exit;

 // ��������� �������. ���� � ��� ���� SeDebugPrivilege, �� �� �����
 // ��������� � ��������� �������
 // �������� ���������� �������� ��� ��� ����������
 hProcess := OpenProcess(PROCESS_TERMINATE, FALSE, dwPID);
 if hProcess =0 then exit;
 // ��������� �������
 if not TerminateProcess(hProcess, DWORD(-1))
  then exit;
 CloseHandle( hProcess );

 // ������� ����������
 //tkp.Privileges[0].Attributes := 0;
 //AdjustTokenPrivileges(hToken, FALSE, tkp, SizeOf(tkp), tkp, ReturnLength);
 //if GetLastError() < > ERROR_SUCCESS
 //then exit;

 Result:=true;
 end;


procedure TAThttpServer.FormDestroy(Sender: TObject);
begin
KillTask(EsName);
end;

function TAThttpServer.ConnectionCheck(ARequestInfo: TIdHTTPRequestInfo):Boolean;
var
sessionid : string;
i : integer;
CI : ^ConnectionInfo;
begin
  sessionid:= ARequestInfo.Params.values['session_id'];
  if (ConnectionList.Count=0) then
  begin
    result:=false;
    Log(ARequestInfo.RemoteIP+'  Session is dead!!!.');
    exit;
  end;
  for i := 0 to ConnectionList.Count-1 do
  begin
    CI:=ConnectionList[i];
    if ((CI^.IP = ARequestInfo.RemoteIP) and (CI^.sessionID = sessionid)) then
    begin
      result:=true;
      exit;
    end;
  end;
  result:=false;
  Log(CI^.IP+'  Session is dead.');
end;

procedure TAThttpServer.FormCreate(Sender: TObject);
begin
    docroot_edit.Text:= getCurrentDir()+'\www';
    webes_edit.Text:=getCurrentDir()+'\es';
end;

end.


