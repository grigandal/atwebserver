object AThttpServer: TAThttpServer
  Left = 316
  Top = 251
  Width = 435
  Height = 219
  Caption = #1042#1077#1073'-'#1089#1077#1088#1074#1077#1088' '#1082#1086#1084#1087#1083#1077#1082#1089#1072' '#1040#1058'-'#1058#1045#1061#1053#1054#1051#1054#1043#1048#1071
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 212
    Height = 13
    Alignment = taCenter
    Caption = #1042#1077#1073'-'#1089#1077#1088#1074#1077#1088' '#1082#1086#1084#1087#1083#1077#1082#1089#1072' '#1040#1058'-'#1058#1045#1061#1053#1054#1051#1054#1043#1048#1071
  end
  object Label2: TLabel
    Left = 16
    Top = 92
    Width = 190
    Height = 13
    Hint = 
      #1044#1080#1088#1077#1082#1090#1086#1088#1080#1103' '#1089' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1084#1080' '#1092#1072#1081#1083#1072#1084#1080', '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1099#1084#1080' '#1076#1083#1103' '#1088#1072#1073#1086#1090#1099' '#1074#1077 +
      #1073'-'#1048#1069#1057'.'
    Caption = #1057#1083#1091#1078#1077#1073#1085#1072#1103' '#1076#1080#1088#1077#1082#1090#1086#1088#1080#1103' '#1074#1077#1073'-'#1089#1077#1088#1074#1077#1088#1072': '
    ParentShowHint = False
    ShowHint = True
  end
  object Label3: TLabel
    Left = 16
    Top = 123
    Width = 107
    Height = 13
    Hint = #1044#1080#1088#1077#1082#1090#1086#1088#1080#1103' '#1089' '#1087#1088#1086#1090#1086#1090#1080#1087#1086#1084' '#1074#1077#1073'-'#1048#1069#1057'.'
    Caption = #1056#1072#1073#1086#1095#1072#1103' '#1076#1080#1088#1077#1082#1090#1086#1088#1080#1103':'
    ParentShowHint = False
    ShowHint = True
  end
  object Label4: TLabel
    Left = 16
    Top = 155
    Width = 141
    Height = 13
    Hint = 
      #1042#1088#1077#1084#1103', '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086#1077' '#1076#1083#1103' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1073#1080#1073#1083#1080#1086#1090#1077#1082' '#1087#1088#1086#1090#1086#1090#1080#1087#1072' '#1074' '#1087#1072#1084#1103#1090#1100' '#1082#1086#1084 +
      #1087#1100#1102#1090#1077#1088#1072'. ('#1084#1089'.)'
    Caption = #1042#1088#1077#1084#1103' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1073#1080#1073#1083#1080#1086#1090#1077#1082':'
    FocusControl = docroot_edit
    ParentShowHint = False
    ShowHint = True
  end
  object Button: TButton
    Left = 8
    Top = 40
    Width = 409
    Height = 33
    Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1074#1077#1073'-'#1089#1077#1088#1074#1077#1088
    TabOrder = 0
    OnClick = ButtonClick
  end
  object docroot_edit: TEdit
    Left = 208
    Top = 88
    Width = 209
    Height = 21
    Hint = 
      #1044#1080#1088#1080#1082#1090#1086#1088#1080#1103' '#1089' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1084#1080' '#1092#1072#1081#1083#1072#1084#1080', '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1099#1084#1080' '#1076#1083#1103' '#1088#1072#1073#1086#1090#1099' '#1074#1077 +
      #1073'-'#1048#1069#1057'.'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Text = 'c:\webes\www'
  end
  object webes_edit: TEdit
    Left = 208
    Top = 120
    Width = 209
    Height = 21
    Hint = #1044#1080#1088#1080#1082#1090#1086#1088#1080#1103' '#1089' '#1087#1088#1086#1090#1086#1090#1080#1087#1086#1084' '#1074#1077#1073'-'#1048#1069#1057'.'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Text = 'c:\webes\es'
  end
  object waittime_edit: TEdit
    Left = 208
    Top = 152
    Width = 209
    Height = 21
    Hint = 
      #1042#1088#1077#1084#1103', '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086#1077' '#1076#1083#1103' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1073#1080#1073#1083#1080#1086#1090#1077#1082' '#1087#1088#1086#1090#1086#1090#1080#1087#1072' '#1074' '#1087#1072#1084#1103#1090#1100' '#1082#1086#1084 +
      #1087#1100#1102#1090#1077#1088#1072'. ('#1084#1089'.)'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    Text = '2000'
  end
  object WebServer: TIdHTTPServer
    Bindings = <>
    CommandHandlers = <>
    Greeting.NumericCode = 0
    MaxConnectionReply.NumericCode = 0
    ReplyExceptionCode = 0
    ReplyTexts = <>
    ReplyUnknownCommand.NumericCode = 0
    TerminateWaitTime = 50000
    ServerSoftware = 'TestServer'
    OnCommandGet = WebServerCommandGet
    Left = 8
    Top = 8
  end
end
